﻿using ClientesAPI.API;
using ClientesAPI.Helpers;
using ClientesAPI.Models;

namespace ClientesAPI.Code
{
    /// <summary>
    /// Camada de regra de negócios
    /// </summary>
    public class PessoaFisicaBLL
    {
        public bool InserirPessoaFisica(PessoaFisica pessoaFisica)
        {
            Utils utils  = new Utils();
            PessoaFisicaDAL dAL = new PessoaFisicaDAL();
            pessoaFisica.Endereco = utils.GerarEndereco(pessoaFisica.Endereco.Cep);
            ValidarPessoaFisica(pessoaFisica);

            return dAL.InserirPessoaFisica(pessoaFisica);
        }
        
        public bool AtualizarPessoaFisica(PessoaFisica pessoaFisica)
        {
            Utils utils = new Utils();
            PessoaFisicaDAL dAL = new PessoaFisicaDAL();
            pessoaFisica.Endereco = utils.GerarEndereco(pessoaFisica.Endereco.Cep);
            ValidarPessoaFisica(pessoaFisica);

            return dAL.AtualizarPessoaFisica(pessoaFisica);
        }
        
        public bool DeletarPessoaFisica(string Cpf)
        {
            Utils utils = new Utils();
            PessoaFisicaDAL dAL = new PessoaFisicaDAL();

            if (!utils.CpfValido(Cpf))
            {
                throw new Exception("Cpf inválido!");
            }

            return dAL.DeletarPessoaFisica(Cpf);
        }
        
        public List<PessoaFisica> ConsultarPessoaFisica(string Cpf)
        {
            Utils utils = new Utils();
            PessoaFisicaDAL dAL = new PessoaFisicaDAL();

            if (!utils.CpfValido(Cpf) && !string.IsNullOrEmpty(Cpf))
            {
                throw new Exception("Cpf inválido!");
            }

            return dAL.ConsultarPessoaFisica(Cpf);
        }

        public void ValidarPessoaFisica(PessoaFisica pessoaFisica)
        {
            Utils utils = new Utils();
            var validacao = utils.ValidarCampos(pessoaFisica.Email, pessoaFisica.Telefone);
            if (!utils.CpfValido(pessoaFisica.Cpf))
            {
                validacao = new Validacao
                {
                    IsValido = false,
                    Mensagem = "Cpf inválido!"
                };
            }

            if (!validacao.IsValido)
            {
                throw new Exception(validacao.Mensagem);
            }
        }
    }
}
