﻿using System.Text;

namespace ClientesAPI.Code
{
    /// <summary>
    /// Classe para armazenamento das queries
    /// </summary>
    public class PessoaFisicaDALSQL
    {
        public string InserirPessoaFisica()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" INSERT INTO PessoaFisica( ");
            sql.AppendLine(" 		Cpf                ");
            sql.AppendLine(" 		,Nome              ");
            sql.AppendLine(" 		,Cep               ");
            sql.AppendLine(" 		,Cidade            ");
            sql.AppendLine(" 		,Estado            ");
            sql.AppendLine(" 		,Logradouro        ");
            sql.AppendLine(" 		,Bairro            ");
            sql.AppendLine(" 		,Email             ");
            sql.AppendLine(" 		,Telefone          ");
            sql.AppendLine(" 		,Classificacao     ");
            sql.AppendLine(" 	) VALUES (             ");
            sql.AppendLine(" 		 @Cpf              ");
            sql.AppendLine(" 		,@Nome             ");
            sql.AppendLine(" 		,@Cep              ");
            sql.AppendLine(" 		,@Cidade           ");
            sql.AppendLine(" 		,@Estado           ");
            sql.AppendLine(" 		,@Logradouro       ");
            sql.AppendLine(" 		,@Bairro           ");
            sql.AppendLine(" 		,@Email            ");
            sql.AppendLine(" 		,@Telefone         ");
            sql.AppendLine(" 		,@Classificacao    ");
            sql.AppendLine(" 	)                      ");

            return sql.ToString();
        }
        
        
        public string AtualizarPessoaFisica()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" UPDATE PessoaFisica	                 ");
            sql.AppendLine("     SET  Nome = @Nome                   ");
            sql.AppendLine("         ,Cep = @Cep                     ");
            sql.AppendLine("         ,Cidade = @Cidade               ");
            sql.AppendLine("         ,Estado = @Estado               ");
            sql.AppendLine("         ,Logradouro = @Logradouro       ");
            sql.AppendLine("         ,Bairro = @Bairro               ");
            sql.AppendLine("         ,Email = @Email                 ");
            sql.AppendLine("         ,Telefone = @Telefone           ");
            sql.AppendLine("         ,Classificacao = @Classificacao ");
            sql.AppendLine("     WHERE Cpf = @Cpf                    ");

            return sql.ToString();
        }
        
        public string DeletarPessoaFisica()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" DELETE FROM PessoaFisica ");
            sql.AppendLine(" 	WHERE Cpf = @Cpf      ");

            return sql.ToString();
        }
        
        public string ConsultarPessoaFisica(string Cpf)
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT               ");
            sql.AppendLine(" 		Cpf           ");
            sql.AppendLine(" 		,Nome         ");
            sql.AppendLine(" 		,Cep          ");
            sql.AppendLine(" 		,Cidade       ");
            sql.AppendLine(" 		,Estado       ");
            sql.AppendLine(" 		,Logradouro   ");
            sql.AppendLine(" 		,Bairro       ");
            sql.AppendLine(" 		,Email        ");
            sql.AppendLine(" 		,Telefone     ");
            sql.AppendLine(" 		,Classificacao");
            sql.AppendLine("  FROM PessoaFisica	  ");
            if (!string.IsNullOrEmpty(Cpf))
            {
                sql.AppendLine("     WHERE Cpf = @Cpf   ");
            }            

            return sql.ToString();
        }
    }
}