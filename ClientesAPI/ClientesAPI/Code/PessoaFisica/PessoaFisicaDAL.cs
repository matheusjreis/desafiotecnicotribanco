﻿using ClientesAPI.Data;
using ClientesAPI.Models;
using System.Data;
using System.Data.SqlClient;

namespace ClientesAPI.Code
{
    /// <summary>
    /// Classe de conexão ao banco de dados
    /// </summary>
    public class PessoaFisicaDAL : BancoDados
    {
        public bool InserirPessoaFisica(PessoaFisica pessoaFisica)
        {
            string sqlCommand = new PessoaFisicaDALSQL().InserirPessoaFisica();
            bool sucesso = false;            

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                command.Parameters.AddWithValue("@Cpf", pessoaFisica.Cpf);
                command.Parameters.AddWithValue("@Nome", pessoaFisica.Nome);
                command.Parameters.AddWithValue("@Cep", pessoaFisica.Endereco.Cep);
                command.Parameters.AddWithValue("@Cidade", pessoaFisica.Endereco.Cidade);
                command.Parameters.AddWithValue("@Estado", pessoaFisica.Endereco.Estado);
                command.Parameters.AddWithValue("@Logradouro", pessoaFisica.Endereco.Logradouro);
                command.Parameters.AddWithValue("@Bairro", pessoaFisica.Endereco.Bairro);
                command.Parameters.AddWithValue("@Email", pessoaFisica.Email);
                command.Parameters.AddWithValue("@Telefone", pessoaFisica.Telefone);
                command.Parameters.AddWithValue("@Classificacao", pessoaFisica.Classificacao);

                sucesso = command.ExecuteNonQuery() > 0;

                command.Connection.Close();
            }

            return sucesso;
        }
        
        public bool AtualizarPessoaFisica(PessoaFisica pessoaFisica)
        {
            string sqlCommand = new PessoaFisicaDALSQL().AtualizarPessoaFisica();
            bool sucesso = false;            

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                command.Parameters.AddWithValue("@Nome", pessoaFisica.Nome);
                command.Parameters.AddWithValue("@Cep", pessoaFisica.Endereco.Cep);
                command.Parameters.AddWithValue("@Cidade", pessoaFisica.Endereco.Cidade);
                command.Parameters.AddWithValue("@Estado", pessoaFisica.Endereco.Estado);
                command.Parameters.AddWithValue("@Logradouro", pessoaFisica.Endereco.Logradouro);
                command.Parameters.AddWithValue("@Bairro", pessoaFisica.Endereco.Bairro);
                command.Parameters.AddWithValue("@Email", pessoaFisica.Email);
                command.Parameters.AddWithValue("@Telefone", pessoaFisica.Telefone);
                command.Parameters.AddWithValue("@Classificacao", pessoaFisica.Classificacao);
                command.Parameters.AddWithValue("@Cpf", pessoaFisica.Cpf);

                sucesso = command.ExecuteNonQuery() > 0;

                command.Connection.Close();
            }

            return sucesso;
        }
        
        public bool DeletarPessoaFisica(string Cpf)
        {
            string sqlCommand = new PessoaFisicaDALSQL().DeletarPessoaFisica();
            bool sucesso = false;            

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                command.Parameters.AddWithValue("@Cpf", Cpf);

                sucesso = command.ExecuteNonQuery() > 0;

                command.Connection.Close();
            }

            return sucesso;
        }
        

        public List<PessoaFisica> ConsultarPessoaFisica(string Cpf)
        {
            string sqlCommand = new PessoaFisicaDALSQL().ConsultarPessoaFisica(Cpf);
            List<PessoaFisica> retorno = new List<PessoaFisica>();

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                if (!string.IsNullOrEmpty(Cpf))
                {
                    command.Parameters.AddWithValue("@Cpf", Cpf);
                }                

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        retorno.Add(new PessoaFisica
                        {
                            Nome = reader["Nome"].ToString(),
                            Endereco = new Endereco
                            {
                                Cep = reader["Cep"].ToString(),
                                Cidade = reader["Cidade"].ToString(),
                                Estado = reader["Estado"].ToString(),
                                Logradouro = reader["Logradouro"].ToString(),
                                Bairro = reader["Bairro"].ToString()
                            },
                            Email = reader["Email"].ToString(),
                            Telefone = reader["Telefone"].ToString(),
                            Classificacao = Convert.ToInt32(reader["Classificacao"]),
                            Cpf = reader["Cpf"].ToString()
                        });
                    }
                }

                command.Connection.Close();
            }

            return retorno;
        }
    }
}