﻿using ClientesAPI.Data;
using ClientesAPI.Models;
using System.Data;
using System.Data.SqlClient;

namespace ClientesAPI.Code
{
    /// <summary>
    /// Classe de conexão ao banco de dados
    /// </summary>
    public class PessoaJuridicaDAL : BancoDados
    {
        public bool InserirPessoaJuridica(PessoaJuridica PessoaJuridica)
        {
            string sqlCommand = new PessoaJuridicaDALSQL().InserirPessoaJuridica();
            bool sucesso = false;

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                command.Parameters.AddWithValue("@Cnpj", PessoaJuridica.Cnpj);
                command.Parameters.AddWithValue("@RazaoSocial", PessoaJuridica.RazaoSocial);
                command.Parameters.AddWithValue("@Cep", PessoaJuridica.Endereco.Cep);
                command.Parameters.AddWithValue("@Cidade", PessoaJuridica.Endereco.Cidade);
                command.Parameters.AddWithValue("@Estado", PessoaJuridica.Endereco.Estado);
                command.Parameters.AddWithValue("@Logradouro", PessoaJuridica.Endereco.Logradouro);
                command.Parameters.AddWithValue("@Bairro", PessoaJuridica.Endereco.Bairro);
                command.Parameters.AddWithValue("@Email", PessoaJuridica.Email);
                command.Parameters.AddWithValue("@Telefone", PessoaJuridica.Telefone);
                command.Parameters.AddWithValue("@Classificacao", PessoaJuridica.Classificacao);

                sucesso = command.ExecuteNonQuery() > 0;

                command.Connection.Close();
            }

            return sucesso;
        }

        public bool AtualizarPessoaJuridica(PessoaJuridica PessoaJuridica)
        {
            string sqlCommand = new PessoaJuridicaDALSQL().AtualizarPessoaJuridica();
            bool sucesso = false;

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                command.Parameters.AddWithValue("@RazaoSocial", PessoaJuridica.RazaoSocial);
                command.Parameters.AddWithValue("@Cep", PessoaJuridica.Endereco.Cep);
                command.Parameters.AddWithValue("@Cidade", PessoaJuridica.Endereco.Cidade);
                command.Parameters.AddWithValue("@Estado", PessoaJuridica.Endereco.Estado);
                command.Parameters.AddWithValue("@Logradouro", PessoaJuridica.Endereco.Logradouro);
                command.Parameters.AddWithValue("@Bairro", PessoaJuridica.Endereco.Bairro);
                command.Parameters.AddWithValue("@Email", PessoaJuridica.Email);
                command.Parameters.AddWithValue("@Telefone", PessoaJuridica.Telefone);
                command.Parameters.AddWithValue("@Classificacao", PessoaJuridica.Classificacao);
                command.Parameters.AddWithValue("@Cnpj", PessoaJuridica.Cnpj);

                sucesso = command.ExecuteNonQuery() > 0;

                command.Connection.Close();
            }

            return sucesso;
        }

        public bool DeletarPessoaJuridica(string Cnpj)
        {
            string sqlCommand = new PessoaJuridicaDALSQL().DeletarPessoaJuridica();
            bool sucesso = false;

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                command.Parameters.AddWithValue("@Cnpj", Cnpj);

                sucesso = command.ExecuteNonQuery() > 0;

                command.Connection.Close();
            }

            return sucesso;
        }


        public List<PessoaJuridica> ConsultarPessoaJuridica(string Cnpj)
        {
            string sqlCommand = new PessoaJuridicaDALSQL().ConsultarPessoaJuridica(Cnpj);
            List<PessoaJuridica> retorno = new List<PessoaJuridica>();

            using (SqlConnection connection = new SqlConnection(base.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand(sqlCommand, connection);

                command.Connection.Open();

                if (!string.IsNullOrEmpty(Cnpj))
                {
                    command.Parameters.AddWithValue("@Cnpj", Cnpj);
                }

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        retorno.Add(new PessoaJuridica
                        {
                            RazaoSocial = reader["RazaoSocial"].ToString(),
                            Endereco = new Endereco
                            {
                                Cep = reader["Cep"].ToString(),
                                Cidade = reader["Cidade"].ToString(),
                                Estado = reader["Estado"].ToString(),
                                Logradouro = reader["Logradouro"].ToString(),
                                Bairro = reader["Bairro"].ToString()
                            },
                            Email = reader["Email"].ToString(),
                            Telefone = reader["Telefone"].ToString(),
                            Classificacao = Convert.ToInt32(reader["Classificacao"]),
                            Cnpj = reader["Cnpj"].ToString()
                        });
                    }
                }

                command.Connection.Close();
            }

            return retorno;
        }
    }
}