﻿using System.Text;

namespace ClientesAPI.Code
{
    /// <summary>
    /// Classe para armazenamento das queries
    /// </summary>
    public class PessoaJuridicaDALSQL
    {
        public string InserirPessoaJuridica()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" INSERT INTO PessoaJuridica( ");
            sql.AppendLine(" 		Cnpj                 ");
            sql.AppendLine(" 		,RazaoSocial         ");
            sql.AppendLine(" 		,Cep                 ");
            sql.AppendLine(" 		,Cidade              ");
            sql.AppendLine(" 		,Estado              ");
            sql.AppendLine(" 		,Logradouro          ");
            sql.AppendLine(" 		,Bairro              ");
            sql.AppendLine(" 		,Email               ");
            sql.AppendLine(" 		,Telefone            ");
            sql.AppendLine(" 		,Classificacao       ");
            sql.AppendLine(" 	) VALUES (               ");
            sql.AppendLine(" 		 @Cnpj               ");
            sql.AppendLine(" 		,@RazaoSocial        ");
            sql.AppendLine(" 		,@Cep                ");
            sql.AppendLine(" 		,@Cidade             ");
            sql.AppendLine(" 		,@Estado             ");
            sql.AppendLine(" 		,@Logradouro         ");
            sql.AppendLine(" 		,@Bairro             ");
            sql.AppendLine(" 		,@Email              ");
            sql.AppendLine(" 		,@Telefone           ");
            sql.AppendLine(" 		,@Classificacao      ");
            sql.AppendLine(" 	)                        ");

            return sql.ToString();
        }


        public string AtualizarPessoaJuridica()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" UPDATE   PessoaJuridica	             ");
            sql.AppendLine("     SET  RazaoSocial = @RazaoSocial     ");
            sql.AppendLine("         ,Cep = @Cep                     ");
            sql.AppendLine("         ,Cidade = @Cidade               ");
            sql.AppendLine("         ,Estado = @Estado               ");
            sql.AppendLine("         ,Logradouro = @Logradouro       ");
            sql.AppendLine("         ,Bairro = @Bairro               ");
            sql.AppendLine("         ,Email = @Email                 ");
            sql.AppendLine("         ,Telefone = @Telefone           ");
            sql.AppendLine("         ,Classificacao = @Classificacao ");
            sql.AppendLine("     WHERE Cnpj = @Cnpj                  ");

            return sql.ToString();
        }

        public string DeletarPessoaJuridica()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" DELETE FROM PessoaJuridica ");
            sql.AppendLine(" 	WHERE Cnpj = @Cnpj      ");

            return sql.ToString();
        }

        public string ConsultarPessoaJuridica(string Cnpj)
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT                 ");
            sql.AppendLine(" 		Cnpj            ");
            sql.AppendLine(" 		,RazaoSocial    ");
            sql.AppendLine(" 		,Cep            ");
            sql.AppendLine(" 		,Cidade         ");
            sql.AppendLine(" 		,Estado         ");
            sql.AppendLine(" 		,Logradouro     ");
            sql.AppendLine(" 		,Bairro         ");
            sql.AppendLine(" 		,Email          ");
            sql.AppendLine(" 		,Telefone       ");
            sql.AppendLine(" 		,Classificacao  ");
            sql.AppendLine("  FROM PessoaJuridica	");
            if (!string.IsNullOrEmpty(Cnpj))
            {
                sql.AppendLine("     WHERE Cnpj = @Cnpj   ");
            }

            return sql.ToString();
        }
    }
}