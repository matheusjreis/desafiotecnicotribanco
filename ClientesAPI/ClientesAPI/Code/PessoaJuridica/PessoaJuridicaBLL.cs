﻿using ClientesAPI.API;
using ClientesAPI.Helpers;
using ClientesAPI.Models;

namespace ClientesAPI.Code.Cliente
{
    /// <summary>
    /// Camada de regra de negócios
    /// </summary>
    public class PessoaJuridicaBLL
    {
        public bool InserirPessoaJuridica(PessoaJuridica pessoaJuridica)
        {
            Utils utils = new Utils();
            Validacao validacao = new Validacao();
            PessoaJuridicaDAL dAL = new PessoaJuridicaDAL();
            pessoaJuridica.Endereco = utils.GerarEndereco(pessoaJuridica.Endereco.Cep);
            ValidarPessoaJuridica(pessoaJuridica);

            return dAL.InserirPessoaJuridica(pessoaJuridica);
        }

        public bool AtualizarPessoaJuridica(PessoaJuridica pessoaJuridica)
        {
            Utils utils = new Utils();
            PessoaJuridicaDAL dAL = new PessoaJuridicaDAL();
            pessoaJuridica.Endereco = utils.GerarEndereco(pessoaJuridica.Endereco.Cep);
            ValidarPessoaJuridica(pessoaJuridica);

            return dAL.AtualizarPessoaJuridica(pessoaJuridica);
        }

        public bool DeletarPessoaJuridica(string Cnpj)
        {
            Utils utils = new Utils();
            PessoaJuridicaDAL dAL = new PessoaJuridicaDAL();

            if (!utils.CnpjValido(Cnpj))
            {
                throw new Exception("Cnpj inválido!");
            }

            return dAL.DeletarPessoaJuridica(Cnpj);
        }

        public List<PessoaJuridica> ConsultarPessoaJuridica(string Cnpj)
        {
            Utils utils = new Utils();
            PessoaJuridicaDAL dAL = new PessoaJuridicaDAL();

            if (!utils.CnpjValido(Cnpj) && !string.IsNullOrEmpty(Cnpj))
            {
                throw new Exception("Cnpj inválido!");
            }

            return dAL.ConsultarPessoaJuridica(Cnpj);
        }

        public void ValidarPessoaJuridica(PessoaJuridica pessoaJuridica)
        {
            Utils utils = new Utils();
            var validacao = utils.ValidarCampos(pessoaJuridica.Email, pessoaJuridica.Telefone);
            if (!utils.CnpjValido(pessoaJuridica.Cnpj))
            {
                validacao = new Validacao
                {
                    IsValido = false,
                    Mensagem = "Cnpj inválido!"
                };
            }

            if (!validacao.IsValido)
            {
                throw new Exception(validacao.Mensagem);
            }
        }
    }
}
