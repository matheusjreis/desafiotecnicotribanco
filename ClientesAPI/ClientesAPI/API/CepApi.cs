﻿using System.Text;
using System.Net;
using System.Text.Json;
using ClientesAPI.Models;

namespace ClientesAPI.API
{
    public class CepApi
    {
        public RetornoCepApi? ConsultarCep(string cep)
        {
            try
            {
                var url = $"https://viacep.com.br/ws/{cep}/json/";
                var request = WebRequest.Create(url);

                request.Method = "GET";

                using var webResponse = request.GetResponse();
                using var webStream = webResponse.GetResponseStream();

                using var reader = new StreamReader(webStream);
                var data = reader.ReadToEnd();

                return JsonSerializer.Deserialize<RetornoCepApi>(data);
            }
            catch (Exception ex)
            {
                throw new Exception($"Erro ao consultar cep: {ex.Message}");
            }
        }
    }
}
