﻿using ClientesAPI.Code;
using ClientesAPI.Code.Cliente;
using ClientesAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ClientesAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PessoaJuridicaController : ControllerBase
    {
        [HttpGet]
        [Route("ConsultarPessoaJuridica")]
        public IActionResult ConsultarPessoaJuridica(string? Cnpj)
        {
            try
            {
                var retorno = new PessoaJuridicaBLL().ConsultarPessoaJuridica(Cnpj);
                if (!retorno.Any())
                {
                    return StatusCode((int)HttpStatusCode.NoContent);
                }

                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }

        [HttpPost]
        [Route("InserirPessoaJuridica")]
        public IActionResult InserirPessoaJuridica(PessoaJuridica PessoaJuridica)
        {
            try
            {
                var retorno = new PessoaJuridicaBLL().InserirPessoaJuridica(PessoaJuridica);
                if (!retorno)
                {
                    return BadRequest("Não foi possível inserir informação de pessoa jurídica");
                }
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }

        [HttpPut]
        [Route("AtualizarPessoaJuridica")]
        public IActionResult AtualizarPessoaJuridica(PessoaJuridica PessoaJuridica)
        {
            try
            {
                var retorno = new PessoaJuridicaBLL().AtualizarPessoaJuridica(PessoaJuridica);
                if (!retorno)
                {
                    return BadRequest("Não foi possível atualizar informação de pessoa jurídica");
                }
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }

        [HttpDelete]
        [Route("DeletarPessoaJuridica")]
        public IActionResult DeletarPessoaJuridica(string Cnpj)
        {
            try
            {
                var retorno = new PessoaJuridicaBLL().DeletarPessoaJuridica(Cnpj);
                if (!retorno)
                {
                    return BadRequest("Não foi possível deletar informação de pessoa jurídica");
                }
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }
    }
}