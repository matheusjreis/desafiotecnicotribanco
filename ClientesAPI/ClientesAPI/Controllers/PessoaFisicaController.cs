using ClientesAPI.Code;
using ClientesAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ClientesAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PessoaFisicaController : ControllerBase
    {
        [HttpGet]
        [Route("ConsultarPessoaFisica")]
        public IActionResult ConsultarPessoaFisica(string? Cpf)
        {
            try
            {
                var retorno = new PessoaFisicaBLL().ConsultarPessoaFisica(Cpf);
                if (!retorno.Any())
                {
                    return StatusCode((int)HttpStatusCode.NoContent);
                }

                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }
        
        [HttpPost]
        [Route("InserirPessoaFisica")]
        public IActionResult InserirPessoaFisica(Models.PessoaFisica pessoaFisica)
        {
            try
            {
                var retorno = new PessoaFisicaBLL().InserirPessoaFisica(pessoaFisica);
                if (!retorno)
                {
                    return BadRequest("N�o foi poss�vel inserir informa��o de pessoa f�sica");
                }
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }
        
        [HttpPut]
        [Route("AtualizarPessoaFisica")]
        public IActionResult AtualizarPessoaFisica(Models.PessoaFisica pessoaFisica)
        {
            try
            {
                var retorno = new PessoaFisicaBLL().AtualizarPessoaFisica(pessoaFisica);
                if (!retorno)
                {
                    return BadRequest("N�o foi poss�vel atualizar informa��o de pessoa f�sica");
                }
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }
        
        [HttpDelete]
        [Route("DeletarPessoaFisica")]
        public IActionResult DeletarPessoaFisica(string Cpf)
        {
            try
            {
                var retorno = new PessoaFisicaBLL().DeletarPessoaFisica(Cpf);
                if (!retorno)
                {
                    return BadRequest("N�o foi poss�vel deletar informa��o de pessoa f�sica");
                }
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, $"Erro: {e.Message} Inner:{e.InnerException?.Message}");
            }
        }
    }
}