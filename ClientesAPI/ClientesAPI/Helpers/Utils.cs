﻿using ClientesAPI.API;
using ClientesAPI.Models;
using System.Text.RegularExpressions;

namespace ClientesAPI.Helpers
{
    public class Utils
    {
        public Endereco GerarEndereco(string cep)
        {
            cep = string.IsNullOrEmpty(cep) ? cep : cep.Replace("-", "");

            var enderecoViaCepApi = new CepApi().ConsultarCep(cep);

            if (enderecoViaCepApi == null)
            {
                throw new Exception($"Endereço do cep {cep} não existe");
            }
            else
            {
                return new Endereco()
                {
                    Cep = enderecoViaCepApi.cep,
                    Cidade = enderecoViaCepApi.localidade,
                    Estado = enderecoViaCepApi.uf,
                    Logradouro = enderecoViaCepApi.logradouro,
                    Bairro = enderecoViaCepApi.bairro
                };
            }

        }

        public bool EmailValido(string email)
        {
            string regex = @"^[^@\s]+@[^@\s]+\.(com|net|org|gov|br)$";

            return Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
        }

        public bool TelefoneValido(string telefone)
        {
            string regex = @"^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$";

            return Regex.IsMatch(telefone, regex, RegexOptions.IgnoreCase);
        }
        
        public bool CpfValido(string cpf)
        {
            string regex = @"^\d{3}\.\d{3}\.\d{3}-\d{2}$";

            if (string.IsNullOrEmpty(cpf))
            {
                return false;
            }                

            return Regex.IsMatch(cpf, regex, RegexOptions.IgnoreCase);
        }
        
        public bool CnpjValido(string cnpj)
        {
            string regex = @"^\d{2}\.\d{3}\.\d{3}/\d{4}-\d{2}$";

            if (string.IsNullOrEmpty(cnpj))
            {
                return false;
            }
            return Regex.IsMatch(cnpj, regex, RegexOptions.IgnoreCase);
        }

        public Validacao ValidarCampos(string email, string telefone)
        {
            var formatoEmailCorreto = EmailValido(email);
            var formatoTelefoneCorreto = TelefoneValido(telefone);
            Validacao validacao = new Validacao
            {
                IsValido = true
            };

            if (!formatoEmailCorreto)
            {
                validacao = new Validacao
                {
                    IsValido = false,
                    Mensagem = "Formato de email incorreto!"
                };
            }

            if (!formatoTelefoneCorreto)
            {
                validacao = new Validacao
                {
                    IsValido = false,
                    Mensagem = "Formato de telefone incorreto!"
                };
            }

            return validacao;
        }
    }
}
