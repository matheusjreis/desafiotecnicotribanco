﻿namespace ClientesAPI.Data
{
    public class BancoDados
    {
        public IConfiguration Configuration { get; set; }

        public string GetConnectionString()
        {
            var path = GerarCaminhoDiretorio();
            
            return $"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={path}\\DatabaseCliente.mdf;Integrated Security=True";
        }

        public string GerarCaminhoDiretorio()
        {
            var todosDiretorios = Directory.GetCurrentDirectory().Split("\\").ToList();
            var caminho = "";
            todosDiretorios.RemoveAt(todosDiretorios.Count - 1);

            foreach (var diretorio in todosDiretorios)
            {
                caminho += $"{diretorio}\\";
            }

            return $"{caminho}DataBase";
        }
    }
}
