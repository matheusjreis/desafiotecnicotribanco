﻿namespace ClientesAPI.Models
{
    public class Endereco
    {
        /// <summary>
        /// Cep do endereco
        /// </summary>
        public string Cep { get; set; }               
        
        /// <summary>
        /// Logradouro do endereco
        /// </summary>
        public string Logradouro { get; set; }

        /// <summary>
        /// Bairro do endereco
        /// </summary>
        public string Bairro { get; set; }

        /// <summary>
        /// Cidade do endereco
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// Estado do endereco
        /// </summary>
        public string Estado { get; set; }
    }
}
