﻿namespace ClientesAPI.Models
{
    public class PessoaFisica : Cliente
    {
        /// <summary>
        /// CPF do cliente
        /// </summary>
        public string Cpf { get; set; }

        /// <summary>
        /// Nome do cliente
        /// </summary>
        public string Nome { get; set; }
    }
}
