﻿namespace ClientesAPI.Models
{
    public class Cliente
    {
        /// <summary>
        /// Email do cliente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone do cliente
        /// </summary>
        public string Telefone { get; set; }

        /// <summary>
        /// Enereço do cliente
        /// </summary>
        public Endereco Endereco { get; set; }

        /// <summary>
        /// CLassificacao
        /// </summary>
        public int Classificacao { get; set; }
    }
}