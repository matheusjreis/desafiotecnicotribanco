﻿namespace ClientesAPI.Models
{
    public class PessoaJuridica : Cliente
    {
        /// <summary>
        /// CPNJ do Cliente
        /// </summary>
        public string Cnpj { get; set; }

        /// <summary>
        /// Razão Social
        /// </summary>
        public string RazaoSocial { get; set; }
    }
}
