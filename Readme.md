## ClientesAPI

### Observações:

- Feita utlizando utlizando .NET 6.0;
- Projetado em 3 (ou 2) camadas (BLL, DAL e/ou DALSQL);

### Funcionalidades:

- Exemplo entradas aceitas (no caso de campos com validações):
    - CPF → 333.333.136-59
    - CNPJ → 68.398.932/0001-78
    - Telefone → (34) 98824-0098
    - Email → [teste@gmail.com](mailto:teste@gmail.com)